source ../settings.sh

echo "Convert example files."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/local" \
	$IMAGENAME \
		--data="json(/local/input/test-data.json)" \
		--source-root=/local/templates \
		--output-root=/local/output \
		--output-encoding=utf8
