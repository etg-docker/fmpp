echo "Testing bash script."
echo

../bash/fmpp-docker \
		--data="json(/local/input/test-data.json)" \
		--source-root=/local/templates \
		--output-root=/local/output \
		--output-encoding=utf8
