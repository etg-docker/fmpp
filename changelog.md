# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Added

- bash-script for calling fmpp: `fmpp-docker`
- test script for bash script

### Changed

- calling image with user- and group id to avoid access permission issues
- reformatted test script - no behaviour change


## [1.0.0] - [0.9.16-2.3.30] - 2021-02-24

- initial version
- fmpp: `0.9.16`
- freemarker: `2.3.30`

### Added

- fmpp docker image
- test files
- CI/CD for gitlab
